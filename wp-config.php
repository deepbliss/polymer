<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'polymerall' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0S)=Hyh{hchgMy B*^v1nzJn{}0X_0s,:`!(jNyz63dsshK9toT3fCdWv%OP5AO;' );
define( 'SECURE_AUTH_KEY',  '*lEr/(PSiK~+S]dDx]HWQMaUZ5a=vZa=0%Y5nDz?O%+#:+GZexjsfDq8p+NL$~ue' );
define( 'LOGGED_IN_KEY',    'w((V7[LGGDV`E.H<R^jLtVA?^?sBPx3pax^_V]W-U-sP>Nr)&i{Sjfo*VmS=~jGG' );
define( 'NONCE_KEY',        '^Hp&Bu9[/bmhnOeM^*Xhy~BDpNam?`qlcAIKY%60cR0l!KN5$(ff_TMECk>&:xa`' );
define( 'AUTH_SALT',        '1v[{%M?%LmW}GkWxJs_RFRkNJo7Su(*.N+kXj{kQo 1aR|VSF(tv%uA^v*mLvSjQ' );
define( 'SECURE_AUTH_SALT', 'QO[=Y9X`j.wxX>|;$ue~)Z~Vah&?_cPJin mi-n|g+)`9azr)JGu=W^MTY6g@NJ8' );
define( 'LOGGED_IN_SALT',   'u+D1*%>fUW!.M!GW_WxO 9t{?YF>4w5Y5^,gI5yS)/E>g,o}M@.l4z64`w9Q8^{G' );
define( 'NONCE_SALT',       '!1bty4wMa^D+:bBdWITHvCp#_K$2*%zG 9?#REXzQ*KMz~,<,uwT.dA[6Fs:Fj;8' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
