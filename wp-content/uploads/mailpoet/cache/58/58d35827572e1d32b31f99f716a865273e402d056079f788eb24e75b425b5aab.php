<?php

use MailPoetVendor\Twig\Environment;
use MailPoetVendor\Twig\Error\LoaderError;
use MailPoetVendor\Twig\Error\RuntimeError;
use MailPoetVendor\Twig\Markup;
use MailPoetVendor\Twig\Sandbox\SecurityError;
use MailPoetVendor\Twig\Sandbox\SecurityNotAllowedTagError;
use MailPoetVendor\Twig\Sandbox\SecurityNotAllowedFilterError;
use MailPoetVendor\Twig\Sandbox\SecurityNotAllowedFunctionError;
use MailPoetVendor\Twig\Source;
use MailPoetVendor\Twig\Template;

/* settings/woocommerce.html */
class __TwigTemplate_eb10cc7cc50aeb11c39f752fddfaf395d4dd451ddbd15dbcef3c2ec666b3174a extends \MailPoetVendor\Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<table class=\"form-table\">
  ";
        // line 2
        if ($this->getAttribute(($context["settings"] ?? null), "woo_commerce_list_sync_enabled", [])) {
            // line 3
            echo "  <tr>
    <th scope=\"row\">
      <label for=\"settings[woocommerce_optin_on_checkout]\">
        ";
            // line 6
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translateWithContext("Opt-in on checkout", "settings area: add an email opt-in checkbox on the checkout page (e-commerce websites)");
            echo "
      </label>
      <p class=\"description\">
        ";
            // line 9
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Customers can subscribe to the \"WooCommerce Customers\" list via a checkbox on the checkout page.");
            echo "
      </p>
    </th>
    <td>
      <p>
        <input
        data-toggle=\"mailpoet_woocommerce_optin_on_checkout\"
        type=\"checkbox\"
        value=\"1\"
        id=\"settings[woocommerce_optin_on_checkout]\"
        name=\"woocommerce[optin_on_checkout][enabled]\"
        ";
            // line 20
            if ($this->getAttribute($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "woocommerce", []), "optin_on_checkout", []), "enabled", [])) {
                echo "checked=\"checked\"";
            }
            // line 21
            echo "        >
      </p>
    </td>
  </tr>
  <tr id=\"mailpoet_woocommerce_optin_on_checkout\">
    <th scope=\"row\">
      <label for=\"settings[woocommerce_checkbox_optin_message]\">
        ";
            // line 28
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translateWithContext("Checkbox opt-in message", "settings area: set the email opt-in message on the checkout page (e-commerce websites)");
            echo "
      </label>
      <p class=\"description\">
        ";
            // line 31
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("This is the checkbox message your customers will see on your WooCommerce checkout page to subscribe to the \"WooCommerce Customers\" list.");
            echo "
      </p>
    </th>
    <td>
      <p>
        <input type=\"text\"
          id=\"woocommerce_checkbox_optin_message\"
          name=\"woocommerce[optin_on_checkout][message]\"
          value=\"";
            // line 39
            echo \MailPoetVendor\twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "woocommerce", []), "optin_on_checkout", []), "message", []), "html", null, true);
            echo "\"
          placeholder=\"";
            // line 40
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translateWithContext("Checkbox opt-in message", "placeholder text for the WooCommerce checkout opt-in message");
            echo "\"
          class=\"regular-text\" />
          <br>
          <div id=\"settings_woocommerce_optin_on_checkout_error\" class=\"mailpoet_error_item mailpoet_error\">
            ";
            // line 44
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("The checkbox opt-in message cannot be empty.");
            echo "
          </div>
      </p>
    </td>
  </tr>
  ";
        }
        // line 50
        echo "  <tr>
    <th scope=\"row\">
      <label for=\"settings[mailpoet_subscribe_old_woocommerce_customers]\">
        ";
        // line 53
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Subscribe old WooCommerce customers");
        echo "
      </label>
      <p class=\"description\">
        ";
        // line 56
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Subscribe all my past customers to this list because they agreed to receive marketing emails from me.");
        echo "
      </p>
    </th>
    <td>
      <p>
        <input
        type=\"checkbox\"
        value=\"1\"
        id=\"settings[mailpoet_subscribe_old_woocommerce_customers]\"
        name=\"mailpoet_subscribe_old_woocommerce_customers[enabled]\"
        ";
        // line 66
        if ($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mailpoet_subscribe_old_woocommerce_customers", []), "enabled", [])) {
            echo "checked=\"checked\"";
        }
        // line 67
        echo "        >
        ";
        // line 77
        echo "        <input
          type=\"hidden\"
          value=\"1\"
          name=\"mailpoet_subscribe_old_woocommerce_customers[dummy]\"
        >
      </p>
    </td>
  </tr>
</table>
";
    }

    public function getTemplateName()
    {
        return "settings/woocommerce.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 77,  138 => 67,  134 => 66,  121 => 56,  115 => 53,  110 => 50,  101 => 44,  94 => 40,  90 => 39,  79 => 31,  73 => 28,  64 => 21,  60 => 20,  46 => 9,  40 => 6,  35 => 3,  33 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "settings/woocommerce.html", "D:\\xampp\\htdocs\\project\\Polymerall\\wp-content\\plugins\\mailpoet\\views\\settings\\woocommerce.html");
    }
}
