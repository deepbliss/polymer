<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header('front'); ?>
	<div class="home_product">
			<ul>
				<?php if( have_rows('home_product') ):?>
				<?php while ( have_rows('home_product') ) : the_row();?>
				<li>
					<div class="front">
						<div class="image">
							<figure>
								<span>
									<img src="<?php the_sub_field('product_image');?>">
								</span>
							</figure>
						</div>
						<h2><?php the_sub_field('product_title');?></h2>
					</div>
					<div class="back">
						<div class="pro_desc">
							<h2><?php the_sub_field('product_title');?></h2>	
							<p><?php the_sub_field('pro_description');?></p>
							<?php $terms = get_sub_field('category_link');?>
							<div class="order_btn"><a href="<?php echo get_category_link( get_sub_field( 'product_link' ) )?>">BROWSE STYLES</a></div>
						</div>
					</div>
				</li>
				<?php endwhile;?>
				<?php endif;?>
			</ul>
		</div>
	</div>

	<div class="home_services">
		<div class="container">
			<h2>Our Services</h2>
			<span>VALUE ADDED</span>
			<ul>
				<?php if( have_rows('home_services') ):
					while ( have_rows('home_services') ) : the_row();?>
				<li>
					<div class="circle">
						<figure>
							<span>
								<img class="service_img" src="<?php the_sub_field('service_image');?>">
								<img class="service_img_hover" src="<?php the_sub_field('service_hover_image');?>">
							</span>
						</figure>
					</div>	
					<h3><?php the_sub_field('service_title');?></h3>
				</li>
				<?php endwhile;endif;?>
			</ul>
			<div class="order_btn"><a href="">Get a Quote</a></div>
		</div>
	</div>

	<div class="video_section">
		<div class="container">
			<div class="video"><iframe allowFullScreen="allowFullScreen" src="https://www.youtube.com/embed/eaoMWyEsehI?ecver=1&amp;iv_load_policy=1&amp;yt:stretch=16:9&amp;autohide=1&amp;color=red&amp;width=633&amp;width=633" width="600" height="315" allowtransparency="true" frameborder="0"><div></div><div></div></iframe></div>
			<div class="video_content"><?php the_field('home_video_content');?></div>
		</div>
	</div>

	<div class="home_featured">
		<div class="container">
			<h2>Featured Products</h2>
			<ul id="featured" class="owl-carousel">
				<?php 
					$args = array(
				    'post_type' => 'product',
				    'posts_per_page' => 4,
				    'order'       => 'ASC' ,
				    'tax_query' => array(
				            array(
				                'taxonomy' => 'product_visibility',
				                'field'    => 'name',
				                'terms'    => 'featured',
				            ),
				        ),
				    );
				$loop = new WP_Query( $args );
				if ( $loop->have_posts() ) {
   				 while ( $loop->have_posts() ) : $loop->the_post();
				?>
				<li>
					<div class="featured_border">
						<div class="featured-img">
							<figure><span>
							<?php  if ( has_post_thumbnail( $loop->post->ID ) ) 
	                    echo get_the_post_thumbnail( $loop->post->ID); ?></span></figure>
						</div>
						<div class="featured-content">
							<div class="featured-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
							<div class="featured-price"><a href="<?php the_permalink(); ?>"><?php echo $product->get_price_html(); ?></a></div>
						</div>
					</div>
					<div class="add_cart_btn">
						<form class="cart" method="post" enctype='multipart/form-data'><?php 
                                        //    do_action( 'woocommerce_before_add_to_cart_button' );
                                         do_action( 'woocommerce_after_shop_loop_item' ); ?>
                                    </form>
					</div>
				</li>
			<?php endwhile;}
			wp_reset_postdata();?>
			</ul>
		</div>		
	</div>

	<div class="our_client">
		<div class="container">
			<div class="client_lft">
				<h2>What Our Clients Are Saying</h2>	
			</div>
			<div class="client_rgt">
				<?php the_field('client_testimonial');?>
			</div>	
		</div>
	</div>


<?php
get_footer();
