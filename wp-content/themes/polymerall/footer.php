<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		
	
<footer id="colophon" class="site-footer">
    <div class="container">

        <!---footer-main--->      
      <div class="footer-main"> 
      <?php //get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
      <!---footer-menu--->
        <div class="footer-menu">
          <?php dynamic_sidebar( 'footer-sec1' ); ?>
          </div>
        <!---footer-menu--->
        <!---footer-location--->
          <div class="footer-location">
          <?php dynamic_sidebar( 'footer-sec2' ); ?>
          </div>
        <!---footer-location--->
        <!---footer-subscribe--->
        <div class="footer-subscribe">
         <?php dynamic_sidebar( 'footer-sec3' ); ?>
        </div>
        <!---footer-subscribe--->
      </div>  
    <!---footer-main--->  
      <div class="site-info">
               <p>Copyrights Polymerall.com. All rights reserved  |  Website design by <a target="_blank" href="https://iovista.com/">ioVista, Inc.</a></p>
      </div><!-- .site-info -->
     </div>
  </footer><!-- #colophon -->
</div>

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.3.min.js"></script>  
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mmenu.min.all.js"></script>
<script type="text/javascript" src="<?php //echo get_template_directory_uri(); ?>/js/easy-responsive-tabs.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.responsiveTabs.js
"></script>
<script>
$(function() {
$('nav#menu').mmenu({
"extensions": [
                  "fx-menu-zoom",
                  "pagedim-black"
               ],
           
            navbars:[ {
                position: 'top',
                content: ['prev', 'title', 'close']
            }
           
            ]
               

});
});
</script>
<script>
	// get video element id
var vidClip = document.getElementById("myVideo"); 

// play video event
function playVid() { 
    myVideo.play();
} 

// pause video event
function pauseVid() { 
  myVideo.pause(); 
}

// toggle button class on click
$('button').on('click', function() {
  $('.first, .second').toggle();
});

// toggle button class when finished
vidClip.onended = function(e) {
  $('.first, .second').toggle();
};
</script>
<script type="text/javascript">
  
  $(window).scroll(function(){
  var sticky = $('.header_bottom'),
      scroll = $(window).scrollTop();

  if (scroll >= 200) sticky.addClass('fixed');
  else sticky.removeClass('fixed');
});
</script>
<script>
function openSearch() {
  document.getElementById("myOverlay").style.display = "block";
}

function closeSearch() {
  document.getElementById("myOverlay").style.display = "none";
}
</script>

<script>
$(document).ready(function() {
$('#featured').owlCarousel({
loop: true,
autoplay: false,
smartSpeed: 1500,
rewindNav:false,
nav: true,
margin:45,
items:4,
dots: false,
responsiveClass:true,
responsive:{
        1024:{
            items:4,
            nav:true
        },
        768:{
            items:3,
            nav:true
        },
        640:{
            items:2,
            nav:true,
            margin:40
        },
        414:{
            items:2,
            nav:true,
            loop:false,
            margin:30
        },
        320:{
            items:1,
            nav:true,
            loop:false,
            margin:30
        }
    }

});
});
</script>

<script>
$(document).ready(function() {
$('#related').owlCarousel({
loop: true,
autoplay: false,
smartSpeed: 1500,
rewindNav:false,
nav: true,
margin:45,
items:4,
dots: false,
responsiveClass:true,
responsive:{
        1024:{
            items:4,
            nav:true
        },
        768:{
            items:3,
            nav:true
        },
        640:{
            items:2,
            nav:true,
            margin:40
        },
        414:{
            items:2,
            nav:true,
            loop:false,
            margin:30
        },
        320:{
            items:1,
            nav:true,
            loop:false,
            margin:30
        }
    }

});
});
</script>







<script type="text/javascript">
$(document).ready(function () {
    
jQuery(".review-link a").click(function(){
   jQuery("#tab2").click();
});    
jQuery('#parentHorizontalTab').responsiveTabs({
rotate: false,
startCollapsed: 'accordion',
collapsible: 'accordion',
setHash: false,
//disabled: [3,4],
activate: function(e, tab) {
$('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
}
});
});
</script>
</body>
</html>
