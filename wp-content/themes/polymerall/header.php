<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/mediaquery.css"> 
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mmenu.all.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/woocommerce.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/woocommerce-smallscreen.css" media="only screen and (max-width: 767px)">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/easy-responsive-tabs.css">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">
<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet"> 
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="wrapper">
	<div class="common_header">	
	<header>
			<div class="header-top">
				<div class="container">
					<div class="header-left">
						<div class="header-phone">
							<img src="<?php echo get_template_directory_uri(); ?>/images/phone_call.png"><span><?php the_field('header_phone', 'option'); ?></span>
						</div>
						<div class="header-mail">
							<img src="<?php echo get_template_directory_uri(); ?>/images/mail.png"><span><a href="mailto:sales@polymerall.com"><?php the_field('header_email', 'option'); ?></a></span>
						</div>
					</div>
					<div class="header-right">
						<span>Keep in touch</span>
						<ul><li><a target="_blank" href="<?php the_field('facebook_link', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png"></a></li>
							<li><a target="_blank" href="<?php the_field('twitter_link', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png"></a></li>
							<li><a target="_blank" href="<?php the_field('linked_in_link', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/linked_in.png"></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="header-top-mobile">
				<div class="container">
					<div class="header-left">
						<div class="header-phone">
							<a href="tel:<?php the_field('header_phone', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/phone_call.png"></a>
						</div>
						<div class="header-mail">
							<a href="mailto:sales@polymerall.com"><img src="<?php echo get_template_directory_uri(); ?>/images/mail.png"><span></a>
						</div>
					</div>
					<div class="header-right">
						<!-- <span>Keep in touch</span> -->
						<ul><li><a target="_blank" href="<?php the_field('facebook_link', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png"></a></li>
							<li><a target="_blank" href="<?php the_field('twitter_link', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png"></a></li>
							<li><a target="_blank" href="<?php the_field('linked_in_link', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/linked_in.png"></a></li>
						</ul>
					</div>
				</div>
			</div>



			<div class="header_bottom">
				
				<div class="container">
					<a class="menu-btn" href="#menu"></a>
					<div class="logo">
						<a class="normal_logo" href="<?php echo site_url();?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a>
						<a class="sticky_logo" href="<?php echo site_url();?>"><img src="<?php echo get_template_directory_uri(); ?>/images/sticky_logo.png"></a>
					</div>

					<div class="header_menu">
						<?php wp_nav_menu(array('menu' => 'Header Menu'));?>	
					</div>
					<div class="mob-nav">
						<nav id="menu">
	                    	<?php wp_nav_menu(array('menu' => 'Header Menu'));?>
	                	</nav>
	                </div>
					<div class="header_right">
						<div class="search_bar">
							<div id="myOverlay" class="overlay">
							  <span class="closebtn" onclick="closeSearch()" title="Close Overlay">×</span>
							  <div class="overlay-content">
							    <!-- <form action="/action_page.php">
							      <input type="text" placeholder="Search.." name="search">
							      <button type="submit"><i class="fa fa-search"></i></button>
							    </form> -->

							<form role="search" method="get" class="" action="http://192.168.1.18/project/Polymerall/">
	
	<input type="text" id="" class="search-field" placeholder="Search" value="sfsfsf" name="s" />
	<button type="submit" class=""><img src="<?php echo get_template_directory_uri(); ?>/images/search.png"></button>
</form>

							  </div>
							</div>
							<div class="openBtn" onclick="openSearch()"><img class="normal_search" src="<?php echo get_template_directory_uri(); ?>/images/search.png"><img class="sticky_search" src="<?php echo get_template_directory_uri(); ?>/images/sticky_search.png"></div>
							<script>
							function openSearch() {
							  document.getElementById("myOverlay").style.display = "block";
							}

							function closeSearch() {
							  document.getElementById("myOverlay").style.display = "none";
							}
							</script>
						</div>
						<div class="user">
							<a class="normal_user" href="<?php echo site_url();?>/my-account"><img src="<?php echo get_template_directory_uri(); ?>/images/user.png"></a>
							<a class="sticky_user" href="<?php echo site_url();?>/my-account"><img src="<?php echo get_template_directory_uri(); ?>/images/sticky_user.png"></a>
						</div>
						<div class="header_cart">
							<a class="normal_cart" href="<?php echo site_url();?>/cart"><img src="<?php echo get_template_directory_uri(); ?>/images/cart.png"></a>
							<a class="sticky_cart" href="<?php echo site_url();?>/cart"><img src="<?php echo get_template_directory_uri(); ?>/images/sticky_cart.png"></a>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="video_bg">
				<div id="video_overlays"></div>
					<video id="myVideo" autoplay loop>
	  					<source src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/20160805_102659_477.mp4" type="video/mp4">
					</video>
					<div class="video_content">
						<div class="buttons">
						  <button class="uk-button uk-button-primary first" onclick="playVid()" type="button">
						    <img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/play.png"></button>
						  <button class="uk-button uk-button-primary second" onclick="pauseVid()" type="button">
						    <img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/pause.png"></button>
						</div>
					</div>	
				</div>
			</div> -->
		</header>
	</div>


