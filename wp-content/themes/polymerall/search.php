<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
</div>


<section class="search-page blog">
<div class="container">
	<div class="right-bar">
			<div class="main-content">
<h1 class="page-title"><?php printf( __( 'Search Results for: %s', '' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		<?php if ( have_posts() ) :
			while ( have_posts() ) : the_post(); ?>
            <div class="blog-block" id="post-<?php the_ID(); ?>">

        <div class="blog-txt-wrap">
        <h2><a href="<?php the_permalink() ?>"><?php the_title( ); ?></a></h2>
       
        <?php $output = get_the_excerpt(); if($output != '') {?>
        <div class="blog-txt"><p><?php the_excerpt(); ?></p></div>
        <?php } ?>
        <div class="blog-btm"><a class="btn btn-primary btn-green blog-readmore" href="<?php the_permalink() ?>">Read More</a>
        </div>
        </div>
        </div>
			<?php endwhile; ?>
		 <?php if(paginate_links()) { ?>
    <div class="pagination">
     <?php //wp_paginate(); ?>
     </div>
     <?php } ?>

		<?php else : ?>

			<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
	<?php endif; ?>
</div>
</div>
</div>
</section>

<?php get_footer();
