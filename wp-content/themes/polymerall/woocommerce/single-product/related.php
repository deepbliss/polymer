<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $related_products ) : ?>

	<section class="related products">

		<h2><?php esc_html_e( 'You May Also Like', 'woocommerce' ); ?></h2>

		<ul class="owl-carousel related" id="featured">
           <?php
           foreach ( $related_products as $related_product ) :
               ?>
                <li>
                	<div class="featured_border">
						<div class="featured-img">
                      		<figure><span><?php if (has_post_thumbnail( $related_product->id )) echo get_the_post_thumbnail($related_product->id, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="" />'; ?></span>
                      		</figure>
                  		</div>
                  	  	<div class="featured-content">
                  	  		<div class="featured-title"><a href="<?php the_permalink($related_product->id);  ?>"><?php echo $related_product->get_title(); ?></a></div>
							<div class="featured-price"><a href="<?php the_permalink($related_product->id);  ?>"><?php echo $related_product->get_price_html(); ?></a></div>
						</div>
					</div>
					<div class="add_cart_btn">
						<form class="cart" method="post" enctype='multipart/form-data'><?php 
                                        //    do_action( 'woocommerce_before_add_to_cart_button' );
                                         do_action( 'woocommerce_after_shop_loop_item' ); ?>
                                    </form>
					</div>	
              </li>
               <?php
            endforeach; ?>
       </ul>

	</section>

<?php endif;

wp_reset_postdata();
?>
<!--recently-viewed-->
<div class="recently-viewed container-fluid">
    <h2><?php esc_html_e( 'Recently Viewed Items', 'woocommerce' ); ?></h2>


    <?php
    echo do_shortcode("[woocommerce_recently_viewed_products per_page=”5″]  ");
    ?>
</div>

