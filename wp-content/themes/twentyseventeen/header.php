<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/mediaquery.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mmenu.all.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/woocommerce.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/woocommerce-smallscreen.css" media="only screen and (max-width: 767px)">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/easy-responsive-tabs.css">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header>
			<div class="header-top">
				<div class="container">
					<div class="header-left">
						<div class="header-phone">
							<img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/phone_call.png"><span>+1-(682)-237-1130</span>
						</div>
						<div class="header-mail">
							<img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/mail.png"><span><a href="mailto:sales@polymerall.com">sales@polymerall.com</a></span>
						</div>
					</div>
					<div class="header-right">
						<span>Keep in touch</span>
						<ul><li><a target="_blank" href="https://www.facebook.com/polymerall1/"><img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/facebook.png"></a></li>
							<li><a target="_blank" href="https://polymerall1/"><img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/twitter.png"></a></li>
							<li><a target="_blank" href="https://www.linkedin.com/company/polymerall-llc"><img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/linked_in.png"></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="header_bottom">
				<div class="container">
					<div class="logo">
						<a href="http://192.168.1.18/project/Polymerall"><img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/logo.png"></a>
					</div>
					<div class="header_menu">
						<div class="menu-header-menu-container"><ul id="menu-header-menu" class="menu"><li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-33"><a href="http://192.168.1.18/project/Polymerall/">Home</a></li>
<li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a href="http://192.168.1.18/project/Polymerall/products/">Products</a></li>
<li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a href="http://192.168.1.18/project/Polymerall/service/">Service</a></li>
<li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="http://192.168.1.18/project/Polymerall/gallery/">Gallery</a></li>
<li id="menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38"><a href="http://192.168.1.18/project/Polymerall/contact-us/">Contact Us</a></li>
</ul></div>					</div>
					<div class="header_right">
						<div class="search_bar">
							<div id="myOverlay" class="overlay">
							  <span class="closebtn" onclick="closeSearch()" title="Close Overlay">×</span>
							  <div class="overlay-content">
							    <!-- <form action="/action_page.php">
							      <input type="text" placeholder="Search.." name="search">
							      <button type="submit"><i class="fa fa-search"></i></button>
							    </form> -->

							<form role="search" method="get" class="" action="http://192.168.1.18/project/Polymerall/">
	
	<input type="text" id="" class="search-field" placeholder="Search" value="sfsfsf" name="s" />
	<button type="submit" class=""><img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/search.png"></button>
</form>

							  </div>
							</div>
							<div class="openBtn" onclick="openSearch()"><img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/search.png"></div>
							<script>
							function openSearch() {
							  document.getElementById("myOverlay").style.display = "block";
							}

							function closeSearch() {
							  document.getElementById("myOverlay").style.display = "none";
							}
							</script>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="video_bg">
				<div id="video_overlays"></div>
					<video id="myVideo" autoplay loop>
	  					<source src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/20160805_102659_477.mp4" type="video/mp4">
					</video>
					<div class="video_content">
						<div class="buttons">
						  <button class="uk-button uk-button-primary first" onclick="playVid()" type="button">
						    <img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/play.png"></button>
						  <button class="uk-button uk-button-primary second" onclick="pauseVid()" type="button">
						    <img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/pause.png"></button>
						</div>
					</div>	
				</div>
			</div> -->
			<div class="video-container">
				<div id="video_overlays"></div>
      			<video  loop  id="myVideo">
				    <source src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/20160805_102659_477.mp4" type="video/mp4">
				</video>
      			<div class="video_content">
					<div class="buttons">
					  <button class="uk-button uk-button-primary first" onclick="playVid()" type="button">
					    <img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/play.png"></button>
					  <button class="uk-button uk-button-primary second" onclick="pauseVid()" type="button">
					    <img src="http://192.168.1.18/project/Polymerall/wp-content/themes/polymerall/images/pause.png"></button>
					</div>
					<h1></h1>
					<div class="order_btn">
						<a href="#">Order Now</a>
					</div>
				</div>
    		</div>
		</header>

